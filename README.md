Planned Graudation: [Fall] [2024]

Graduation Type: [MS]

Have set research topic: [No]

Percent completed: [0]%

For undergrad/MS students:

Are you planning to publish a peer-reviewed paper? [No]

If yes, do you have a target journal or conference? [No]

Checklist for all:

- [x] Overleaf account

- [x] Comfortable enough with Git to put code and changes in a repo here (emailing code is not allowed)

- [x] On the NSF mailing list

- [x] On the Discord server (optional if really against Discord, email Raphaela to get added otherwise)
